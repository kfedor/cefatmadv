#include <X11/Xlib.h>
#include "cefadvbrowser.h"

Logger logger;

namespace
{
	int XErrorHandlerImpl( Display *display, XErrorEvent *event )
	{
		LOG( WARNING )
		        << "X error received: "
		        << "type " << event->type << ", "
		        << "serial " << event->serial << ", "
		        << "error_code " << static_cast<int>( event->error_code ) << ", "
		        << "request_code " << static_cast<int>( event->request_code ) << ", "
		        << "minor_code " << static_cast<int>( event->minor_code );
		return 0;
	}

	int XIOErrorHandlerImpl( Display *display )
	{
		return 0;
	}

}  // namespace

int main( int argc, char* argv[] )
{		
	try
	{	
		std::unique_ptr<CefAdvBrowser> cefb( new CefAdvBrowser( argc, argv ) );
		
		signal( SIGINT, cefb->signal_callback_handler );
		signal( SIGTERM, cefb->signal_callback_handler );

		XSetErrorHandler( XErrorHandlerImpl );
		XSetIOErrorHandler( XIOErrorHandlerImpl );

		cefb->setSettings();
		cefb->addTimers();
		cefb->run();
		cefb->shutdown();		
	}

	catch( std::exception &ex )
	{
		logger.errorLog( "Runtime error: [%s]\n", ex.what() );
	}

	return 0;
}
