#include <string>

#include "include/cef_browser.h"
#include "include/cef_command_line.h"
#include "include/wrapper/cef_helpers.h"

#include "simple_app.h"

void SimpleApp::OnContextInitialized()
{
	CEF_REQUIRE_UI_THREAD();

	CefWindowInfo window_info;
	handler = new SimpleHandler();

	CefBrowserSettings browser_settings;
	std::string url;

	CefRefPtr<CefCommandLine> command_line = CefCommandLine::GetGlobalCommandLine();

	url = command_line->GetSwitchValue( "url" );

	if( url.empty() )
	{
		url.assign( "file://" );
		url.append( get_current_dir_name() );
		url.append( "/site/index.html" );
	}

	// Create the first browser window.
	CefBrowserHost::CreateBrowser( window_info, handler.get(), url, browser_settings, NULL );
}

void SimpleApp::reload()
{
	handler->reload();
}
