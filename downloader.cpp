#include "downloader.h"
#include "cefadvbrowser.h"

void downloader::thr( downloader *thr )
{
	thr->error = 1;
	thr->is_downloading = 1;

	if( thr->set_download_data() )
	{
		if( thr->do_downloading() )
		{
			if( thr->do_moving() )
			{
				thr->error = 0;
			}
			else
				logger.errorLog( "Downloader::thread: do_moving failed\n" );
		}
		else
			logger.errorLog( "Downloader::thread: do_downloading failed\n" );
	}
	else
		logger.errorLog( "Downloader::thread: set_download_data failed\n" );

	thr->is_downloading = 0;	
	g_idle_add_full( G_PRIORITY_DEFAULT, thr->parent->updateReport, static_cast<gpointer>( thr ), NULL );
}

int downloader::start()
{
	try
	{
		thr_handle = std::thread( thr, this );
		thr_handle.detach();
	}

	catch( const exception &ex )
	{		
		g_idle_add_full( G_PRIORITY_DEFAULT, parent->updateReport, static_cast<gpointer>( this ), NULL );
		logger.errorLog( "Downloader::start() - Error creating thread [error:%d]\n", ex.what() );
		return 0;
	}

	return 1;
}

int downloader::download_file( string filename, string saveto )
{
	string::size_type last = saveto.rfind( '/' );

	if( last != string::npos )
	{
		string dir = saveto.substr( 0, last );
		create_full_path( dir.c_str() );
	}

	string curl_url;
	curl_url.assign( storage.url );
	curl_url.append( "/" );
	curl_url.append( filename );

	logger.errorLog( "Downloader::for download [%s]\n", curl_url.c_str() );

	CURL *curl = curl_easy_init();

	if( curl )
	{
		FILE *outfile = fopen( saveto.c_str(), "wb" );

		if( !outfile )
			return 0;

		curl_easy_setopt( curl, CURLOPT_URL, curl_url.c_str() );

		/*
				if( curl_url.compare( 0, 4, "http" ) == 0 )
				{
					//curl_easy_setopt( curl, CURLOPT_ENCODING, "windows-1251");
				}
		*/
		if( curl_url.compare( 0, 5, "https" ) == 0 )
		{
			curl_easy_setopt( curl, CURLOPT_SSL_VERIFYPEER, 0L );
			curl_easy_setopt( curl, CURLOPT_SSL_VERIFYHOST, 0L );
			//curl_easy_setopt( curl, CURLOPT_ENCODING, "windows-1251");
		}
		else if( curl_url.compare( 0, 4, "sftp" ) == 0 )
		{
			curl_easy_setopt( curl, CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_AGENT );
		}
		else if( curl_url.compare( 0, 4, "ftps" ) == 0 )
		{
			curl_easy_setopt( curl, CURLOPT_SSL_VERIFYPEER, 0L );
			curl_easy_setopt( curl, CURLOPT_SSL_VERIFYHOST, 0L );
			curl_easy_setopt( curl, CURLOPT_USERPWD, storage.login_password.c_str() );
		}
		else
		{
			curl_easy_setopt( curl, CURLOPT_FTP_USE_EPSV, 0 );
			curl_easy_setopt( curl, CURLOPT_USERPWD, storage.login_password.c_str() );
		}

		curl_easy_setopt( curl, CURLOPT_WRITEDATA, outfile );
		curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, my_write_func );
		//curl_easy_setopt( curl, CURLOPT_READFUNCTION, my_read_func );
		//curl_easy_setopt( curl, CURLOPT_VERBOSE, 1L); // logging
		//curl_easy_setopt( curl, CURLOPT_RESUME_FROM, offset );
		curl_easy_setopt( curl, CURLOPT_NOSIGNAL, 1 ); // wah wah

		curl_easy_setopt( curl, CURLOPT_CONNECTTIMEOUT, 30 );
//		curl_easy_setopt( curl, CURLOPT_TIMEOUT, 60L ); // 20 sec

		CURLcode res = curl_easy_perform( curl );

		if( CURLE_OK != res )
		{
			logger.errorLog( "Downloader::Error while downloading file (%s)\n", curl_easy_strerror( res ) );
			curl_easy_cleanup( curl );
			fclose( outfile );
			return 0;
		}

		curl_easy_cleanup( curl );
		fclose( outfile );

		logger.errorLog( "Downloader::Downloaded %s OK !\n", filename.c_str() );

		return 1;
	}

	return 0;
}


int downloader::do_downloading()
{
	FCrc crc;
	int dl_error = 0;

	if( storage.filez.size() == 0 )
	{
		logger.errorLog( "@ downloading file list empty\n" );
		return 1;
	}

	// удалить дубликаты

	sort( storage.filez.begin(), storage.filez.end(), sort_method() );
	vector<_download_file>::iterator new_end;
	new_end = unique( storage.filez.begin(), storage.filez.end(), uniq_method() );
	storage.filez.erase( new_end, storage.filez.end() );

	// поехали

	for( auto i=0; i<storage.filez.size(); i++ )
	{
		//int offset = 0;
		string tmpname;
		string dstname;
		tmpname.assign( storage.dir_tmp );
		tmpname.append( "/" );
		tmpname.append( storage.filez[i].name );

		// конечный каталог
		dstname.assign( storage.dir_root );
		dstname.append( "/" );

		if( storage.filez[i].final_name.length() > 0 )
			dstname.append( storage.filez[i].final_name );
		else
			dstname.append( storage.filez[i].name );


		// Проверяем основной каталог
		if( crc.GetFileCRC32( dstname.c_str() ) == storage.filez[i].crc )
			continue;

		// Проверяем временный каталог
		if( crc.GetFileCRC32( tmpname.c_str() ) == storage.filez[i].crc )
			continue;

		need_restart = 1;
		/* DL start */
		int network_error = 0;

		for( auto j=0; j<=10; j++ )
		{
			logger.errorLog( "---Downloading [%s] в [%s] (try %d)\n", storage.filez[i].name.c_str(), tmpname.c_str(), j );

			string for_download;
			for_download.assign( dlpath );
			for_download.append( storage.filez[i].name );

			if( download_file( for_download, tmpname ) )
			{
				network_error = 0;
				break;
			}

			network_error++;
			sleep( 300 );
		}

		/* DL end */

		if( network_error )
		{
			logger.errorLog( "   '---Error downloading file [%s]\n", storage.filez[i].name.c_str() ); // ошибка связи
			dl_error++;
			continue; // а проверять то нечего
		}

		if( crc.GetFileCRC32( tmpname.c_str() ) != storage.filez[i].crc )
		{
			logger.errorLog( "   '---File downloaded with errors [%s]\n", storage.filez[i].name.c_str() );
			dl_error++;
			unlink( tmpname.c_str() ); // удалить, чтобы не проколоться потом с этим файлом
		}
	} // end for

	if( dl_error == 0 )
		return 1;
	else
		return 0;
}

int downloader::do_moving()
{
	FCrc crc;
	int cp_error = 0;

	if( storage.filez.size() == 0 )
	{
		logger.errorLog( "Downloader::downloading file list empty\n" );
		return 1;
	}

	for( unsigned int i=0; i<storage.filez.size(); i++ )
	{
		// временный каталог
		string tmpname;
		tmpname.assign( storage.dir_tmp );
		tmpname.append( "/" );
		tmpname.append( storage.filez[i].name );

		// конечный каталог
		string dstname;
		dstname.assign( storage.dir_root );
		dstname.append( "/" );

		if( storage.filez[i].final_name.length() > 0 )
			dstname.append( storage.filez[i].final_name );
		else
			dstname.append( storage.filez[i].name );

		uint32_t fcrc32 = crc.GetFileCRC32( dstname.c_str() );

		// если файла нету вообще или crc не совпадает
		if( fcrc32 != storage.filez[i].crc )
		{
			fcrc32 = crc.GetFileCRC32( tmpname.c_str() );

			// если файл есть в dir_TMP и его CRC OK!
			if( fcrc32 != 0 )
			{
				if( fcrc32 == storage.filez[i].crc )
				{
					logger.errorLog( "---Copying [%s] to [%s]\n", tmpname.c_str(), dstname.c_str() );

					need_restart = 1;

					string::size_type last = dstname.rfind( '/' );

					if( last != string::npos )
					{
						string dir = dstname.substr( 0, last );
						create_full_path( dir.c_str() );
					}

					if( rename( tmpname.c_str(), dstname.c_str() ) < 0 )
						cp_error++;
				}
				else
				{
					logger.errorLog( "File [%s] damaged\n", storage.filez[i].name.c_str() );
					cp_error ++;
				}
			} // проверка на наличие файла
		}
	}

	if( cp_error == 0 )
		return 1;

	return 0;
}

int downloader::create_full_path( const char* path )
{
	int len;
	char *new_path = NULL;
	int ret = 1;

	new_path = ( char* ) malloc( strlen( path )+1 );
	memset( new_path, 0, strlen( path )+1 );
	strcpy( new_path,path );

	while( ( len = strlen( new_path ) ) && new_path[len - 1] == '/' )
		new_path[len - 1] = 0;

	while( mkdir( new_path,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ) == -1 )
	{
		char *slash;

		if( errno == EEXIST )
			break;

		if( errno != ENOENT )
		{
			ret = 0;
			break;
		}

		if( !( slash = strrchr( new_path,'/' ) ) )
		{
			ret = 0;
			break;
		}

		len = slash - new_path;
		new_path[len] = 0;

		if( !create_full_path( new_path ) )
		{
			ret = 0;
			break;
		}

		new_path[len] = '/';
	}

	free( new_path );
	return ret;
}
