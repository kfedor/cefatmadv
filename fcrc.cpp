#include "fcrc.h"

FCrc::FCrc() : m_crc32( 0 )
{
	initCrcCcittTab();
	init_crc16_tab();
	memset( table, 0, sizeof( table ) );
}

void FCrc::initCrcCcittTab( void )
{
	for( int i=0; i<256; i++ )
	{
		unsigned short crc = 0;
		unsigned short c = ( ( unsigned short ) i ) << 8;

		for( int j=0; j<8; j++ )
		{

			if( ( crc ^ c ) & 0x8000 )
				crc = ( crc << 1 ) ^ P_CCITT;
			else
				crc =   crc << 1;

			c = c << 1;
		}

		crc_tabccitt[i] = crc;
	}
}

unsigned short FCrc::updateCrcCcitt( unsigned short crc, char c )
{
	unsigned short tmp=0x0, short_c=0x0;

	short_c  = 0x00ff & ( unsigned short ) c;

	tmp = ( crc >> 8 ) ^ short_c;
	crc = ( crc << 8 ) ^ crc_tabccitt[tmp];

	return crc;
}

unsigned short FCrc::updateCrc16( unsigned short crc, char c )
{
	unsigned short short_c = 0x00ff & ( unsigned short ) c;
	unsigned short tmp =  crc ^ short_c;
	crc = ( crc >> 8 ) ^ crc_tab16[ tmp & 0xff ];

	return crc;
}

void FCrc::init_crc16_tab( void )
{
	for( int i=0; i<256; i++ )
	{
		unsigned short crc = 0;
		unsigned short c = ( unsigned short ) i;

		for( int j=0; j<8; j++ )
		{
			if( ( crc ^ c ) & 0x0001 ) crc = ( crc >> 1 ) ^ P_16;
			else                      crc =   crc >> 1;

			c = c >> 1;
		}

		crc_tab16[i] = crc;
	}
}

unsigned short FCrc::getCrcCcitt( char *data, int len )
{
	int i;
	unsigned short crc_ccitt_0000 = 0;

	for( i=0; i<len; i++ )
	{
		crc_ccitt_0000 = updateCrcCcitt( crc_ccitt_0000, *data );
		data++;
	}

	return crc_ccitt_0000;
}

unsigned short FCrc::getCrcKermit( char *data, int size )
{
	unsigned short int valuehex;
	unsigned short int CRC;
	const char *pData = ( const char* )( data );

	valuehex = 0;
	CRC = 0;

	for( int i=0; i<size; ++i )
	{
		valuehex = ( ( *pData ^ CRC ) & 0x0fu ) * 0x1081;
		CRC >>= 4;
		CRC ^= valuehex;
		valuehex = ( ( *pData >> 4 ) ^ ( CRC & 0x00ffu ) ) & 0x0fu;
		CRC >>= 4;
		CRC ^= ( valuehex * 0x1081u );

		++pData;
	}

	return ( ( CRC & 0x00ffu ) << 8 ) | ( ( CRC & 0xff00u ) >> 8 );
}

// Pinpad
unsigned short FCrc::getCrc16( char *data, int len )
{
	int i;
	unsigned short crc_16 = 0;

	for( i=0; i<len; i++ )
	{
		crc_16 = updateCrc16( crc_16, *data );
		data++;
	}

	return crc_16;
}

unsigned short FCrc::getCrc16( vector<unsigned char> &data )
{
	unsigned short crc_16 = 0;

	for( unsigned int i=0; i<data.size(); i++ )
	{
		crc_16 = updateCrc16( crc_16, data[i] );
	}

	return crc_16;
}

int FCrc::checkCrcCcitt( unsigned char *data, int len )
{
	unsigned short crc = getCrcCcitt( ( char* )data, len-2 );
	unsigned short crc_net = 0;

	crc_net |= data[len-1] << 8;
	crc_net |= data[len-2];

	if( crc_net == crc )
		return 1;
	else
		logger.errorLog( "FCrc failed comparing [%02X] and [%02X]\n", crc_net, crc );

	return 0;
}

void FCrc::init_table()
{
	const unsigned CRC_POLY = 0xEDB88320;
	unsigned i, j, r;

	for( i = 0; i < 256; i++ )
	{
		for( r = i, j = 8; j; j-- )
			r = r & 1? ( r >> 1 ) ^ CRC_POLY: r >> 1;

		table[i] = r;
	}

	m_crc32 = 0;
}

void FCrc::ProcessCRC( void* pData, register int nLen )
{
	const unsigned int CRC_MASK = 0xD202EF8D;
	register unsigned char* pdata = reinterpret_cast<unsigned char*>( pData );
	register unsigned int crc = m_crc32;

	while( nLen-- )
	{
		crc = table[static_cast<unsigned char>( crc ) ^ *pdata++] ^ crc >> 8;
		crc ^= CRC_MASK;
	}

	m_crc32 = crc;
}

uint64_t FCrc::GetFileSize( const char *filename )
{
	struct stat statbuf;

	if( stat( filename, &statbuf ) == -1 )
	{
		return -1;
	}

	return statbuf.st_size;
}

uint32_t FCrc::GetFileCRC32( const char *filename )
{
	long bytes_read;
	int pfd;
	uint64_t filesize;
	void *filedata;

	init_table();

	filesize = GetFileSize( filename );

	if( filesize == -1 )
		return 0;

	pfd = open( filename, O_RDONLY );

	if( pfd < 0 )
		return 0;

	//cout << "Filesize: " << filesize <<endl;
	if( filesize > 2000000000 )
		return 0xFFFFFFFF; // tmp tbd

	filedata = ( void* )malloc( filesize );

	if( !filedata )
	{
		logger.errorLog( "Malloc error\n" );
		return 0;
	}

	bytes_read = read( pfd, filedata, filesize );

	if( bytes_read < 0 )
	{
		close( pfd );
		free( filedata );
		return 0;
	}

	ProcessCRC( ( void* )filedata, filesize );

	close( pfd );

	free( filedata );

	return m_crc32;
}
