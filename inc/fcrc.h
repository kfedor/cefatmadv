#ifndef _FCRC
#define _FCRC

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "logger.h"

extern Logger logger;

#define P_CCITT  0x1021
#define P_16     0xA001  // crc 16 polynomial X^16+X^15+x^2+X

class FCrc
{
	public:
		FCrc();

		// crc32
		unsigned int m_crc32;
		void ProcessCRC( void* pData, int nLen );

		uint64_t GetFileSize( const char * );
		uint32_t GetFileCRC32( const char * );

		// used for CDM 6240
		unsigned short getCrcCcitt( char *data, int len );

		// Hitachi
		unsigned short getCrcKermit( char *data, int len );

		// used for Zt 596
		unsigned short getCrc16( char *data, int len );
		unsigned short getCrc16( vector<unsigned char> &data );

		int checkCrcCcitt( unsigned char *data, int len );

	private:
		unsigned short crc_tabccitt[256];
		unsigned short crc_tab16[256];
		unsigned int table[256];

		void initCrcCcittTab( void );
		void init_crc16_tab( void );

		// crc16
		unsigned short updateCrcCcitt( unsigned short crc, char c );
		unsigned short updateCrc16( unsigned short crc, char c );

		// crc32
		void init_table();
};

#endif
