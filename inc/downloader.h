#ifndef _DOWNLOADER_H
#define _DOWNLOADER_H

using namespace std;

#include <vector>
#include <string>
#include <algorithm>
#include <thread>

#include <curl/curl.h>
#include <curl/easy.h>
#include <glib.h>
#include <json/value.h>
#include <json/reader.h>

#include "fcrc.h"
#include "logger.h"

#define U_PROGRAM 1
#define U_ADVERT 2
#define U_INTERFACE 3

extern Logger logger;
extern gboolean update_report( gpointer q );

class CefAdvBrowser;

struct _download_file
{
	uint64_t len;
	uint64_t crc;
	string name; // тут содержится имя файла и его путь из XML, по нему будем скачивать
	string final_name; // а тут содержим конечный путь и имя файла..если оно необходимо

	_download_file()
	{
		len = 0;
		crc = 0;
		name.clear();
		final_name.clear();
	};
};

struct _download_struct
{
	string url;
	string login_password;
	string dir_root;  // куда копировать в итоге
	string dir_tmp;  // временный каталог

	std::vector<_download_file> filez;
};

struct uniq_method
{
	bool operator()( _download_file a, _download_file b )
	{
		if( a.name.compare( b.name ) == 0 )
			return 1;
		else
			return 0;
	}
};

struct sort_method
{
	bool operator()( _download_file a, _download_file b )
	{
		return a.name.length() < b.name.length();
	}
};

class downloader
{
	public:
		downloader( CefAdvBrowser *p ) : parent( p ), type( 0 ), error( 1 ), need_restart( 0 ) {}
		virtual ~downloader() {}
		CefAdvBrowser *parent;

		int start();
		virtual int set_download_data()
		{
			return 0;
		}

		int getType()
		{
			return type;
		}
		int getError()
		{
			return error;
		}
		int getRestart()
		{
			return need_restart;
		}
		int isDownloading()
		{
			return is_downloading;
		}

	protected:
		int type;

		string dlpath; // workdir path
		_download_struct storage;

		int download_file( string, string );

		static size_t my_write_func( void *ptr, size_t size, size_t nmemb, FILE *stream )
		{
			return fwrite( ptr, size, nmemb, stream );
		}

	private:
		int error;
		int need_restart;
		int is_downloading;

		std::thread thr_handle;
		static void thr( downloader * );

		int create_full_path( const char* path );
		int do_downloading();
		int do_moving();
};


#endif
