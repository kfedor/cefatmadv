#ifndef FTPUPDATE_PROGRAM_H
#define FTPUPDATE_PROGRAM_H

#include <iostream>
#include <fstream>
#include "downloader.h"

#define DIR_ROOT "."
#define DIR_TMP "program_tmp"
#define INDEX_XML_LOCAL "index.json"

class ftpupdate_program : public downloader
{
	public:
		ftpupdate_program( CefAdvBrowser *p, string url, string lp, string dl ) :
			downloader( p ),
			url_update( url ),
			login_pass( lp ),
			download_path( dl ) {}
			
		int set_download_data();

	private:
		string url_update;
		string login_pass;
		string download_path;
};

#endif
