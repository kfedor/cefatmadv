#ifndef CEF_TESTS_CEFSIMPLE_SIMPLE_APP_H_
#define CEF_TESTS_CEFSIMPLE_SIMPLE_APP_H_

#include "include/cef_app.h"

#include "simple_handler.h"

// Implement application-level callbacks for the browser process.
class SimpleApp : public CefApp, public CefBrowserProcessHandler
{
	public:
		SimpleApp() {}

		// CefApp methods:
		virtual CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() OVERRIDE { return this; }

		// CefBrowserProcessHandler methods:
		virtual void OnContextInitialized() OVERRIDE;
		
		void reload();

	private:
		// Include the default reference counting implementation.
		IMPLEMENT_REFCOUNTING( SimpleApp );
		
		CefRefPtr<SimpleHandler> handler;
};

#endif  // CEF_TESTS_CEFSIMPLE_SIMPLE_APP_H_
