#ifndef _LOGGER_H
#define _LOGGER_H

#pragma GCC diagnostic ignored "-Wwrite-strings"

#include <mutex>
#include <vector>
#include <fstream>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdarg.h>

#define ERRORLOG "./wtm_errorlog"
#define CASHLOG  "./wtm_cashlog"

#define CONSOLE   1
#define NOCONSOLE 0

using namespace std;

class Logger
{
	public:
		void printHex( string some, vector<unsigned char> data );
		void printHex( unsigned char *data, int i );
		void printHex( vector<unsigned char> );
		void errorLog( const char *data, ... );
		void cashLog( char *data, ... );
		void cleanupLogs();
		int getLog( int position, string & );

	private:
		std::mutex logger_errorlog_mutex;
		std::mutex logger_cashlog_mutex;

		void fileLog( char *file, char *data, mutex *mtx, int console );

};


#endif
