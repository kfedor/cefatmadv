#ifndef _CEFADVBROWSER_H
#define _CEFADVBROWSER_H

#include "include/base/cef_logging.h"

#include <thread>
#include <iostream>
#include <memory>
#include <gdk/gdk.h>

#include "ftpupdate_program.h"
#include "simple_app.h"
#include "logger.h"

class CefAdvBrowser
{
	public:
		CefAdvBrowser( int argc, char **argv );

		static gboolean checkUpdate( CefAdvBrowser* );
		static gboolean updateReport( gpointer ptr );
		static void signal_callback_handler( int signum );

		void setSettings();
		void addTimers();
		void run();
		void reload();
		void shutdown();

	private:
		unsigned int is_downloading;

		std::unique_ptr<ftpupdate_program> updater;
		CefRefPtr<SimpleApp> app;
		CefMainArgs main_args;
		CefSettings settings;
};

#endif
