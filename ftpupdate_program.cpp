#include "ftpupdate_program.h"

int ftpupdate_program::set_download_data()
{
	Json::Value root;
	Json::Reader reader;
	ifstream in;

	type = U_PROGRAM;
	_download_file item;

	storage.filez.clear();
	storage.url.assign( url_update );

	/* new */
	storage.login_password.assign( login_pass );

	storage.dir_root.assign( DIR_ROOT );
	storage.dir_tmp.assign( DIR_TMP );
	dlpath.assign( download_path );

	if( download_path.length() == 0 )
		return 0;

	string index_json;
	index_json.assign( download_path );
	index_json.append( "index.json" );

	int network_error = 0;

	for( auto j=1; j<=10; j++ )
	{
		logger.errorLog( "Downloading %s/%s (try %d)\n", url_update.c_str(), index_json.c_str(), j );

		if( download_file( index_json, INDEX_XML_LOCAL ) )
		{
			network_error = 0;
			break;
		}

		network_error++;
		sleep( 300 );
	}

	if( network_error )
	{
		logger.errorLog( "Unable to download index.xml\n" );
		return 0;
	}

	in.open( INDEX_XML_LOCAL );

	if( !in.is_open() )
		return 0;

	if( !reader.parse( in, root ) )
	{
		in.close();
		logger.errorLog( "Error in file %s, cancelling application update\n", INDEX_XML_LOCAL );
		return 0;
	}

	in.close();

	const Json::Value files = root["files"];

	if( files.size() <= 0 )
	{
		logger.errorLog( "Error in file %s, cancelling application update\n", INDEX_XML_LOCAL );
		return 0;
	}

	for( unsigned int index = 0; index < files.size(); ++index )
	{
		item.name = files[index].get( "name", 0 ).asString();
		string crc32 = files[index].get( "crc32", 0 ).asString();
		item.crc = strtoul( crc32.c_str(), NULL, 16 );
		item.len = files[index].get( "length", 0 ).asUInt64();

		if( item.name.compare( INDEX_XML_LOCAL ) != 0 || item.name.compare( item.name.length()-7, 7, "jsoncrc" ) == 0 )
			storage.filez.push_back( item );

	}

	return 1;
}
