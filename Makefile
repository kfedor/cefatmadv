GTKLIBS = `pkg-config --cflags --libs gtk+-2.0`
CEFLIBS = -L./bin -lcef_dll_wrapper -lcef
CC = g++ -Wl,-rpath,. -std=c++11 -fPIC
FILES = $(wildcard *.cpp) $(wildcard *.cc)
INC = -I./inc -I./cef_include -I/usr/include/jsoncpp
LIBS = -lcrypto -ljsoncpp -lcurl
OUT = -o bin/cefatmadv

all:
	$(CC) $(INC) $(OUT) $(FILES) $(GTKLIBS) $(CEFLIBS) $(LIBS) -lX11
