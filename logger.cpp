#include "logger.h"

// Possible crash on improper position
int Logger::getLog( int position, string &str )
{
	char filename[256];

	std::lock_guard<std::mutex> lock( logger_errorlog_mutex );

	time_t now_time;
	struct tm wk_localtime;
	struct tm *now;

	time( &now_time );
	now = localtime_r( &now_time, &wk_localtime );

	sprintf( filename, "%s_%02d_%02d_%02d.log",
	         "wtm_errorlog",
	         now->tm_mday,
	         now->tm_mon + 1,
	         1900 + now->tm_year );

	ifstream logfile;
	logfile.open( filename );

	if( !logfile.is_open() )
	{
		cout << "Logger::getLog() - Error opening logfile: " << filename << endl;
		return 0;
	}

	// Get file size
	logfile.seekg( 0, ios::end );
	int size = logfile.tellg();

	// Seek pos
	logfile.seekg( position, ios::beg );

	str.clear();
	str.assign( ( std::istreambuf_iterator<char>( logfile ) ), std::istreambuf_iterator<char>() );

	logfile.close();

	return size;
}

void Logger::printHex( vector<unsigned char> data )
{
	unsigned int sz = data.size();

	printf( "[%u]: ", sz );

	for( unsigned int i=0; i<sz; i++ )
		printf( "%02X ", data[i] );

	printf( "\n" );
}

void Logger::printHex( string some, vector<unsigned char> data )
{
	unsigned int sz = data.size();

	printf( "%s [%u]: ", some.c_str(), sz );

	for( unsigned int i=0; i<sz; i++ )
		printf( "%02X ", data[i] );

	printf( "\n" );
}

void Logger::printHex( unsigned char *data, int i )
{
	int q;

	printf( "[%d]: ", i );

	for( q=0; q<i; q++ )
		printf( "%02X ", data[q] );

	printf( "\n" );
}

void Logger::fileLog( char *file, char *data, mutex *mtx, int console )
{
	FILE *fp;
	char filename[256];

	std::lock_guard<std::mutex> lock( *mtx );

	time_t now_time;
	struct tm wk_localtime;
	struct tm *now;

	time( &now_time );
	now = localtime_r( &now_time, &wk_localtime );

	sprintf( filename, "%s_%02d_%02d_%02d.log",
	         file,
	         now->tm_mday,
	         now->tm_mon + 1,
	         1900 + now->tm_year );

	if( ( fp = fopen( filename, "a" ) ) == NULL )
		return;


	fprintf( fp, "[%02d/%02d/%04d %02d:%02d:%02d] ", now->tm_mday,
	         now->tm_mon + 1, 1900 + now->tm_year, now->tm_hour,
	         now->tm_min, now->tm_sec );

	fprintf( fp, "%s", data );
	fclose( fp );

	if( console )
	{
		printf( "[%02d/%02d/%04d %02d:%02d:%02d] ", now->tm_mday,
		        now->tm_mon + 1, 1900 + now->tm_year, now->tm_hour,
		        now->tm_min, now->tm_sec );

		printf( "%s", data );
	}

	return;
}

void Logger::cashLog( char *data, ... )
{
	char tmp[2048] = {0};
	va_list ap;
	va_start( ap, data );

	vsnprintf( tmp, sizeof( tmp ), data, ap );
	fileLog( CASHLOG, tmp, &logger_cashlog_mutex, NOCONSOLE );

	va_end( ap );
}

void Logger::errorLog( const char *data, ... )
{
	char tmp[2048] = {0};
	va_list ap;
	va_start( ap, data );

	vsnprintf( tmp, sizeof( tmp ), data, ap );
	fileLog( ERRORLOG, tmp, &logger_errorlog_mutex, CONSOLE );

	va_end( ap );
}

void Logger::cleanupLogs()
{
	DIR *dirp;
	struct dirent *dp;
	struct stat     statbuf;
	time_t result;

	result = time( NULL );

	if( ( dirp = opendir( "." ) ) == NULL )
	{
		perror( "couldn’t open ’.’" );
		return;
	}

	do
	{
		errno = 0;

		if( ( dp = readdir( dirp ) ) != NULL )
		{
			if( !strncmp( dp->d_name, "wtm_errorlog_", 15 ) && strstr( dp->d_name, ".log" ) )
			{
				if( stat( dp->d_name, &statbuf ) != -1 )
				{
					if( result-statbuf.st_mtime > 1000000 )
					{
						unlink( dp->d_name );
						errorLog( "Cleaning up old log files [%s]\n", dp->d_name );
					}
				}
			}
		}
	}
	while( dp != NULL );

	if( errno != 0 )
		perror( "error reading directory" );

	( void ) closedir( dirp );
	return;
}
