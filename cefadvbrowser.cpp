#include "cefadvbrowser.h"

CefAdvBrowser::CefAdvBrowser( int argc, char **argv ) : is_downloading( 0 ), main_args( argc, argv )
{
	string urlUpdate( "ftps://api.wtm.cox.ru:990" );
	string lpUpdate( "atmupdate:F4reQWete3" );
	string update_path( "atm/advert/" );

	updater = std::unique_ptr<ftpupdate_program>( new ftpupdate_program( this, urlUpdate, lpUpdate, update_path ) );

	if( CefExecuteProcess( main_args, NULL, NULL ) >= 0 )
		throw runtime_error( "CefExecuteProcess" );
}

void CefAdvBrowser::signal_callback_handler( int signum )
{
	logger.errorLog( "Caught signal %d\n", signum );
	CefQuitMessageLoop();	
}

gboolean CefAdvBrowser::updateReport( gpointer q )
{	
	downloader *dl = static_cast<downloader*>( q );

	if( !dl->getRestart() )
		return FALSE;

	logger.errorLog( "Update report\n" );

	dl->parent->reload();

	return FALSE;
}

gboolean CefAdvBrowser::checkUpdate( CefAdvBrowser *ptr )
{	
	if( ptr->updater->isDownloading() == 0 )
	{
		logger.errorLog( "Checking for update\n" );
		ptr->updater->start();
	}
	else
	{
		logger.errorLog( "Downloading already in process..\n" );
	}

	return TRUE; // continue working
}

void CefAdvBrowser::addTimers()
{
	checkUpdate( this );
	g_timeout_add( 1000*60*5, ( GSourceFunc )checkUpdate, this );
}

void CefAdvBrowser::run()
{
	app = new SimpleApp();
	CefInitialize( main_args, settings, app.get(), NULL );

	CefRunMessageLoop();
}

void CefAdvBrowser::shutdown()
{
	CefShutdown();
}

void CefAdvBrowser::setSettings()
{
	settings.no_sandbox = true;
	settings.multi_threaded_message_loop = false;
	//settings.pack_loading_disabled = true;
}

void CefAdvBrowser::reload()
{
	logger.errorLog( "Reloading browser\n" );
	app->reload();
}
